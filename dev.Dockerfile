FROM golang:1.17.5-stretch

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y git

# working solution 1
RUN go env -w GOPRIVATE=gitlab.com
RUN touch ~/.netrc
RUN chmod 600 ~/.netrc
RUN echo "machine gitlab.com login hamid7126 password glpat-An4YmXhXLohMTLDVPNq1" >> ~/.netrc

RUN go install github.com/fullstorydev/grpcui/cmd/grpcui@latest

WORKDIR /app

RUN curl -fLo install.sh https://raw.githubusercontent.com/cosmtrek/air/master/install.sh \
    && chmod +x install.sh && sh install.sh && cp ./bin/air /bin/air

CMD air