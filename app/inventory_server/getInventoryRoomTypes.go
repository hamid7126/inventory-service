package authserver

import (
	"context"
	"fmt"
	"inventory-service/app/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/hamid7126/auth-grpc/inventorypb"
)

func (s *Server) GetInventoryRoomTypes(c context.Context, req *inventorypb.GetInventoryRoomTypesRequest) (*inventorypb.GetInventoryRoomTypesResponse, error) {

	var itemsPerPage int64 = 5

	if !req.EndDate.AsTime().After(req.StartDate.AsTime()) {
		return nil, fmt.Errorf("end date must be after start data")
	}

	listCursor, err := s.Collections.CRSInventory.Find(context.TODO(), bson.M{"Date": bson.M{"$lte": req.EndDate.AsTime(), "$gte": req.StartDate.AsTime()}, "PropertyCode": req.PropertyCode}, &options.FindOptions{Limit: &itemsPerPage, Sort: bson.M{"Date": 1}})
	// listCursor, err := s.Collections.CRSInventory.Find(context.TODO(), bson.M{"Date": bson.M{"$lte": req.EndDate.AsTime(), "$gte": req.StartDate.AsTime()}}, &options.FindOptions{Sort: bson.M{"Date": 1}})

	if err != nil {
		return nil, err
	}

	inventoryRecords := []*inventorypb.InventoryRecord{}

	for listCursor.Next(context.Background()) {
		elem := &models.CRSInventoryItem{}

		err := listCursor.Decode(elem)

		if err != nil {
			return nil, err
		}

		item := &inventorypb.InventoryRecord{
			Date:         &timestamppb.Timestamp{Seconds: elem.Date.Time().Unix()},
			PropertyCode: elem.PropertyCode,
			RoomTypes:    []*inventorypb.RoomType{},
		}

		for _, element := range elem.RoomTypes {
			singleRoomType := &inventorypb.RoomType{
				Code:   element.GetCode(),
				Name:   element.GetName(),
				NameFa: element.GetNameFA(),
				// DefaultOccupancy: element.DefaultOccupancy,
				Availability: element.GetCRSAvailability(),

				// CrsAvailability
				TotalAllocatedAvailability: element.GetTotalAllocatedAvailability(),
				Reserved:                   element.GetReserved(),
				// PooledAvailabbility
				RatePlans: []*inventorypb.RatePlan{},
			}

			item.RoomTypes = append(item.RoomTypes, singleRoomType)
		}

		inventoryRecords = append(inventoryRecords, item)
	}

	return &inventorypb.GetInventoryRoomTypesResponse{
		InventoryRecords: inventoryRecords,
	}, nil

}
