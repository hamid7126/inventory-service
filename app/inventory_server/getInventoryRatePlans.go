package authserver

import (
	"context"
	"fmt"
	"inventory-service/app/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/hamid7126/auth-grpc/inventorypb"
)

func (s *Server) GetInventoryRatePlans(c context.Context, req *inventorypb.GetInventoryRatePlansRequest) (*inventorypb.GetInventoryRatePlansResponse, error) {

	// var itemsPerPage int64 = 5

	if !req.EndDate.AsTime().After(req.StartDate.AsTime()) {
		return nil, fmt.Errorf("end date must be after start data")
	}

	matchStage := bson.D{{"$match", bson.D{{"Date", bson.D{{"$lte", req.EndDate.AsTime()}, {"$gte", req.StartDate.AsTime()}}}, {"PropertyCode", req.PropertyCode}}}}
	// lookupStage := bson.D{{"$lookup", bson.D{{"RoomTypes.Code", req.RoomTypeCode}}}}
	// unwindeStage := bson.D{{"$unwind", "$RoomTypes"}}
	projectStage := bson.D{{"$project", bson.D{{"Date", 1}, {"RoomTypes", bson.D{{"$filter", bson.D{{"input", "$RoomTypes"}, {"as", "someObj"}, {"cond", bson.D{{"$eq", bson.A{"$$someObj.Code", req.RoomTypeCode}}}}}}}}}}}

	// listCursor, err := s.Collections.CRSInventory.Find(context.TODO(), bson.M{"Date": bson.M{"$lte": req.EndDate.AsTime(), "$gte": req.StartDate.AsTime()}, "PropertyCode": req.PropertyCode, "RoomTypes.Code": req.RoomTypeCode}, &options.FindOptions{Limit: &itemsPerPage, Sort: bson.M{"Date": 1}})
	listCursor, err := s.Collections.CRSInventory.Aggregate(context.TODO(), mongo.Pipeline{matchStage, projectStage})
	// listCursor, err := s.Collections.CRSInventory.Find(context.TODO(), bson.M{"Date": bson.M{"$lte": req.EndDate.AsTime(), "$gte": req.StartDate.AsTime()}}, &options.FindOptions{Sort: bson.M{"Date": 1}})

	if err != nil {
		return nil, err
	}

	inventoryRatePlanRecords := []*inventorypb.InventoryRatePlanRecord{}

	for listCursor.Next(context.Background()) {
		elem := &models.CRSInventoryItem{}

		err := listCursor.Decode(elem)

		if err != nil {
			return nil, err
		}

		item := &inventorypb.InventoryRatePlanRecord{
			Date:      &timestamppb.Timestamp{Seconds: elem.Date.Time().Unix()},
			RatePlans: []*inventorypb.RatePlan{},
		}

		for _, element := range elem.RoomTypes[0].RatePlans {
			singleRatePlan := &inventorypb.RatePlan{
				Code:       element.GetCode(),
				Name:       element.GetName(),
				NameFa:     element.GetNameFA(),
				Multiplier: element.GetMultiplier(),
				Restrictions: &inventorypb.Restrictions{
					Status:           element.GetRestrictions().GetStatus(),
					CloseToArrival:   element.GetRestrictions().GetCloseToArrival(),
					CloseToDeparture: element.GetRestrictions().GetCloseToDeparture(),
					MinStay:          element.GetRestrictions().GetMinStay(),
					MaxStay:          element.GetRestrictions().GetMaxStay(),
				},
				Rates: []*inventorypb.Rate{},
				AddOn: &inventorypb.AddOn{
					AddBed: element.GetAddOn().GetAddBed(),
					Child1: element.GetAddOn().GetChild1(),
					Child2: element.GetAddOn().GetChild2(),
					Child3: element.GetAddOn().GetChild3(),
					Child4: element.GetAddOn().GetChild4(),
				},
				// BreakfastPrice: float64(element.BreakfastPrice.Value),
			}

			if element.Rates != nil {
				for _, singleRate := range element.Rates {
					singleRatePlan.Rates = append(singleRatePlan.Rates, &inventorypb.Rate{
						Amount:    singleRate.Value.Amount,
						Occupancy: singleRate.Value.Occupancy,
						Currency:  inventorypb.Currency(inventorypb.Currency_value[singleRate.Value.Currency]),
					})
				}
			}

			item.RatePlans = append(item.RatePlans, singleRatePlan)
		}

		inventoryRatePlanRecords = append(inventoryRatePlanRecords, item)
	}

	return &inventorypb.GetInventoryRatePlansResponse{
		InventoryRatePlanRecords: inventoryRatePlanRecords,
	}, nil

}
