package authserver

import (
	"inventory-service/app/db"

	"gitlab.com/hamid7126/auth-grpc/inventorypb"
)

type Server struct {
	Collections *db.Collections
	inventorypb.InventoryServiceServer
}
