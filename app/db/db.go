package db

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var client *mongo.Client

type Collections struct {
	CRSInventory *mongo.Collection
}

func ConnectToDB() {

	credentials := options.Credential{
		Username: os.Getenv("DB_USERNAME"),
		Password: os.Getenv("DB_PASSWORD"),
	}

	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017").SetAuth(credentials)

	ctx, cancel := context.WithTimeout(context.Background(), 40*time.Second)
	defer cancel()

	localClient, err := mongo.Connect(ctx, clientOptions)

	client = localClient

	if err != nil {
		log.Println("error connecting to db!!!!!!!!!!!!!!!!!!!")
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(ctx, nil)

	if err != nil {
		log.Fatal(err)
	}

}

func CloseConnectionToDB() {
	if client == nil {
		return
	}

	err := client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal(err)
	}

	// TODO optional you can log your closed MongoDB client
	fmt.Println("Connection to MongoDB closed.")
}

func getDB() *mongo.Database {
	return client.Database("adotel")
}

func GetCollections() *Collections {
	collections := Collections{
		CRSInventory: getCRSInventoryCollection(),
	}

	return &collections
}

// CreateIndex - creates an index for a specific field in a collection
func CreateIndex(coll *mongo.Collection, fields primitive.M, unique bool) {

	// 1. Lets define the keys for the index we want to create
	mod := mongo.IndexModel{
		Keys:    fields, // index in ascending order or -1 for descending order
		Options: options.Index().SetUnique(unique),
	}

	// 2. Create the context for this operation
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// 3. Connect to the database and access the collection

	// 4. Create a single index
	_, err := coll.Indexes().CreateOne(ctx, mod)
	if err != nil {
		// 5. Something went wrong, we log it and return false
		fmt.Println(err.Error())
	}
}
