package db

import (
	"go.mongodb.org/mongo-driver/mongo"
)

func getCRSInventoryCollection() (crsInventoryCollection *mongo.Collection) {
	// collectionNames, _ := getDB().ListCollectionNames(context.TODO(), bson.D{})

	// for _, name := range collectionNames {
	// 	if name == "crs_inventory" {

	// 	}
	// }

	// return generateUsersCollection()

	return getDB().Collection("crs_inventory")
}

// func generateUsersCollection() *mongo.Collection {
// 	collectionOptions := options.CreateCollectionOptions{
// 		Validator: bson.M{
// 			"$jsonSchema": bson.M{
// 				"bsonType": "object",
// 				"required": []string{"name", "email", "password", "role"},
// 				// "additionalProperties": false,
// 				"additionalProperties": false,
// 				"properties": bson.M{
// 					"_id": bson.M{
// 						"bsonType": "objectId",
// 					},
// 					"name": bson.M{
// 						"bsonType":    "string",
// 						"description": "the field name is required",
// 					},
// 					"email": bson.M{
// 						"bsonType": "string",
// 					},
// 					"password": bson.M{
// 						"bsonType": "string",
// 					},
// 					"active": bson.M{
// 						"bsonType": "bool",
// 					},
// 					"role": bson.M{
// 						"bsonType": "string",
// 					},
// 					"entity": bson.M{
// 						"bsonType": "object",
// 						"properties": bson.M{
// 							"entityType": bson.M{
// 								"bsonType": "string",
// 							},
// 							"entityCode": bson.M{
// 								"bsonType": "string",
// 							},
// 						},
// 					},
// 					"unlimitedResources": bson.M{
// 						"bsonType": "bool",
// 					},
// 					"resources": bson.M{
// 						"bsonType": "array",
// 						"items": bson.M{
// 							"bsonType": "object",
// 							"properties": bson.M{
// 								"type": bson.M{
// 									"bsonType": "string",
// 								},
// 								"codes": bson.M{
// 									"bsonType": "array",
// 									"items": bson.M{
// 										"bsonType": "string",
// 									},
// 								},
// 							},
// 						},
// 					},
// 					"lastLoginAt": bson.M{
// 						"bsonType": "timestamp",
// 					},
// 					"createdAt": bson.M{
// 						"bsonType": "timestamp",
// 					},
// 					"updatedAt": bson.M{
// 						"bsonType": "timestamp",
// 					},
// 				},
// 			},
// 		},
// 	}

// 	err := getDB().CreateCollection(context.Background(), "users", &collectionOptions)
// 	if err != nil {
// 		log.Fatal(err)
// 		return nil
// 	}

// 	coll := getDB().Collection("users")

// 	// CreateIndex(coll, bson.M{"email": 1}, true)
// 	// CreateIndex(coll, "role", false)
// 	// CreateIndex(coll, "entity.entityCode", false)

// 	return coll
// }
