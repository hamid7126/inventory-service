package main

import (
	"inventory-service/app/db"
	"log"
	"net"

	inventoryserver "inventory-service/app/inventory_server"

	"github.com/joho/godotenv"
	"gitlab.com/hamid7126/auth-grpc/inventorypb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading environment variables file")
	}
	db.ConnectToDB()
	defer db.CloseConnectionToDB()

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	s := grpc.NewServer()
	inventorypb.RegisterInventoryServiceServer(s, &inventoryserver.Server{Collections: db.GetCollections()})

	reflection.Register(s)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
		return
	}

}
