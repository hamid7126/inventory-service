package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type ValueObjectRate struct {
	Value Rate `bson:"_value,omitempty"`
}

type ValueObjectInt struct {
	Value int `bson:"_value,omitempty"`
}

type CRSInventoryItem struct {
	Date         *primitive.DateTime `bson:"Date,omitempty"`
	PropertyCode string              `bson:"PropertyCode,omitempty"`
	RoomTypes    []*RoomType         `bson:"RoomTypes,omitempty"`
}
