package models

type Rate struct {
	Amount    float64 `bson:"Amount,omitempty"`
	Occupancy uint32  `bson:"Occupancy,omitempty"`
	Currency  string  `bson:"Currency,omitempty"`
}
