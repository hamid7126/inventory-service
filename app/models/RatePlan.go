package models

type RatePlan struct {
	Name                           string             `bson:"Name,omitempty"`
	NameFA                         string             `bson:"NameFA,omitempty"`
	Code                           string             `bson:"Code,omitempty"`
	Multiplier                     float32            `bson:"Multiplier,omitempty"`
	AllocatedAvailability          int32              `bson:"AllocatedAvailability,omitempty"`
	RemainingAllocatedAvailability int32              `bson:"RemainingAllocatedAvailability,omitempty"`
	AddOn                          *AddOn             `bson:"AddOn,omitempty"`
	Restrictions                   *Restrictions      `bson:"Restrictions,omitempty"`
	Rates                          []*ValueObjectRate `bson:"Rates,omitempty"`
	BreakfastPrice                 *ValueObjectInt    `bson:"BreakfastPrice,omitempty"`
}

func (r RatePlan) GetName() string {
	return r.Name
}

func (r RatePlan) GetNameFA() string {
	return r.NameFA
}

func (r RatePlan) GetCode() string {
	return r.Code
}

func (r RatePlan) GetMultiplier() float32 {
	return r.Multiplier
}

func (r RatePlan) GetAllocatedAvailability() float32 {
	return float32(r.AllocatedAvailability)
}

func (r RatePlan) GetRemainingAllocatedAvailability() float32 {
	return float32(r.RemainingAllocatedAvailability)
}

func (r RatePlan) GetAddOn() *AddOn {
	return r.AddOn
}

func (r RatePlan) GetRestrictions() *Restrictions {
	return r.Restrictions
}
