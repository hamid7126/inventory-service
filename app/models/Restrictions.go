package models

type Restrictions struct {
	Status           bool  `bson:"Status,omitempty"`
	CloseToArrival   bool  `bson:"CloseToArrival,omitempty"`
	CloseToDeparture bool  `bson:"CloseToDeparture,omitempty"`
	MinStay          int32 `bson:"MinStay,omitempty"`
	MaxStay          int32 `bson:"MaxStay,omitempty"`
}

func (r *Restrictions) GetStatus() bool {
	if r == nil {
		return false
	}
	return r.Status
}

func (r *Restrictions) GetCloseToArrival() bool {
	if r == nil {
		return false
	}
	return r.CloseToArrival
}

func (r *Restrictions) GetCloseToDeparture() bool {
	if r == nil {
		return false
	}
	return r.CloseToDeparture
}

func (r *Restrictions) GetMinStay() int32 {
	if r == nil {
		return 0
	}
	return r.MinStay
}

func (r *Restrictions) GetMaxStay() int32 {
	if r == nil {
		return 0
	}
	return r.MaxStay
}
