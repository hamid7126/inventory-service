package models

type RoomType struct {
	Code                       string          `bson:"Code,omitempty"`
	Name                       string          `bson:"Name,omitempty"`
	NameFA                     string          `bson:"NameFA,omitempty"`
	CRSAvailability            *ValueObjectInt `bson:"CRSAvailability,omitempty"`
	TotalAllocatedAvailability *ValueObjectInt `bson:"TotalAllocatedAvailability,omitempty"`
	Reserved                   *ValueObjectInt `bson:"Reserved,omitempty"`
	RatePlans                  []*RatePlan     `bson:"RatePlans,omitempty"`
}

func (r RoomType) GetCode() string {
	return r.Code
}

func (r RoomType) GetName() string {
	return r.Name
}

func (r RoomType) GetNameFA() string {
	return r.NameFA
}

func (r RoomType) GetCRSAvailability() uint32 {
	if r.CRSAvailability != nil {
		return uint32(r.CRSAvailability.Value)
	}
	return 0
}

func (r RoomType) GetTotalAllocatedAvailability() uint32 {
	if r.TotalAllocatedAvailability != nil {
		return uint32(r.TotalAllocatedAvailability.Value)
	}
	return 0
}

func (r RoomType) GetReserved() int32 {
	if r.Reserved != nil {
		return int32(r.Reserved.Value)
	}
	return 0
}
