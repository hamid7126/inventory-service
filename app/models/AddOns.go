package models

type AddOn struct {
	AddBed float64 `bson:"AddBed,omitempty"`
	Child1 float64 `bson:"Child1,omitempty"`
	Child2 float64 `bson:"Child2,omitempty"`
	Child3 float64 `bson:"Child3,omitempty"`
	Child4 float64 `bson:"Child4,omitempty"`
}

func (a *AddOn) GetAddBed() float64 {
	if a == nil {
		return 0
	}
	return a.AddBed
}

func (a *AddOn) GetChild1() float64 {
	if a == nil {
		return 0
	}
	return a.Child1
}

func (a *AddOn) GetChild2() float64 {
	if a == nil {
		return 0
	}
	return a.Child2
}

func (a *AddOn) GetChild3() float64 {
	if a == nil {
		return 0
	}
	return a.Child3
}

func (a *AddOn) GetChild4() float64 {
	if a == nil {
		return 0
	}
	return a.Child4
}
